# -*- coding: utf-8 -*-
"""
DATA ENGINEER TEST - ALEXANDER BENITEZ

Este script de Python procesa los archivos Ads_schedule.csv y Analytics_schedule.csv y los sube a GBQ
Adicionalmente crea un SQL statement con las estadisticas del los data set y los muestra en consola
El proceso de las fechas toma un tiempo en realizarse, por lo cual se incluye un archivo HDFS5
Si el archivo store.h5 no esta ubicado en la carpeta de ejecuccion, lee los dataframes desde los archivos

@author: Alexander Benitez
"""
# modulos a utilizar
import pandas as pd
import numpy as np
from datetime import datetime #Para procesar las fechas
import pandas_gbq #Para subir el dataset directamente a GBQ
import fuzzywuzzy
from fuzzywuzzy import process #Para la consistencia de datos

dfSql = pd.DataFrame() # Declarar global universal para generar SQL

# Cargar data en DataFrames, si hay un h5, cargarlos desde ahi, si no, desde csv
try:
    ads_data = pd.read_hdf('store.h5', 'ads_data')
    analytics_data = pd.read_hdf('store.h5', 'analytics_data')
except:
    ads_data = pd.read_csv('Ads_schedule.csv') #Utilizar parametro nrows=90000 para probar, limita las filas a 90000
    analytics_data = pd.read_csv('Analytics.csv') #Utilizar parametro nrows=90000 para probar, limita las filas a 90000

def calcularMetricas():
    global dfSql
    #### VALORES FALTANTES ####
    #verificamos valores faltantes por dataset
    valores_faltantes_ads = ads_data.isnull().sum()
    print('Datos faltantes en Ads Schedule por columna:\n',valores_faltantes_ads, '\n')
    valores_faltantes_analytics = analytics_data.isnull().sum()
    print('Datos faltantes en Analytics por columna:\n',valores_faltantes_analytics, '\n')
    
    print('La forma de Ads es:\n',ads_data.shape, '\n')
    print('La forma de Analytics es:\n',analytics_data.shape, '\n')
    
    celdas_totales_ads = np.product(ads_data.shape)
    print('Celdas totales en Ads:\n',celdas_totales_ads, '\n')
    celdas_totales_faltantes_ads = valores_faltantes_ads.sum()
    print('Celdas faltantes en Ads:\n',celdas_totales_faltantes_ads, '\n')
    
    celdas_totales_analytics = np.product(analytics_data.shape)
    print('Celdas totales en Analytics:\n',celdas_totales_analytics, '\n')
    celdas_totales_faltantes_analytics = valores_faltantes_analytics.sum()
    print('Celdas faltantes en Analytics:\n',celdas_totales_faltantes_analytics, '\n')
    
    #Verificamos porcentaje de data faltantes por dataset
    porcentaje_faltante_ads = (celdas_totales_faltantes_ads/celdas_totales_ads)*100
    print('El porcentaje de data faltante en ads es:\n', "%.2f%%" % porcentaje_faltante_ads, '\n') #Hago print del float con 5 decimales y un % al final
    porcentaje_faltante_analytics = (celdas_totales_faltantes_analytics/celdas_totales_analytics)*100
    print('El porcentaje de data faltante en analytics es:\n', "%.2f%%" % porcentaje_faltante_analytics, '\n') #Hago print del float con 5 decimales y un % al final
    
    #### VALORES DUPLICADOS ####
    proporcion_duplicados_ads = ads_data.duplicated().mean()*100
    print('La proporcion de filas duplicadas en ads es:\n', "%.2f%%" % proporcion_duplicados_ads)
    proporcion_duplicados_analytics = analytics_data.duplicated().mean()*100
    print('La proporcion de filas duplicadas en analytics es:\n', "%.2f%%" % proporcion_duplicados_analytics)
    
    dfSql = pd.DataFrame({'Dataset': ['Ads', 'Analytics'],
              'Forma': [ads_data.shape, analytics_data.shape],
              'Celdas_totales': [celdas_totales_ads,celdas_totales_analytics],
              'Celdas_faltantes': [porcentaje_faltante_ads, porcentaje_faltante_analytics],
              'Porcentaje_faltantes': [porcentaje_faltante_ads, porcentaje_faltante_analytics],
              'Proporcion_duplicados': [proporcion_duplicados_ads, proporcion_duplicados_analytics]})

def procesarFechas():
    global ads_data, analytics_data
    #Vamos a realizar parsing de las fechas
    #Verifiquemos las columnas de los dataframe, para ver cual hace referencia a 'date'
    print('Las columnnas de analytics son: \n', analytics_data.columns, '\n')
    print('Las columnnas de ads son: \n', ads_data.columns, '\n')
    
    #Solo hay 1002 NA en start_date, no es significativo, por eso hago drop
    ads_data = ads_data.dropna(subset=['start_date']) 
    
    # Vamos a procesar las fechas de ads, start_date, date_loaded y end_date 
    # Como hay casos en que el mes esta primero, hago un try y en caso de que este, trata con el mes primero
    for index, row in ads_data.iterrows():
        try:
            ads_data.loc[index, 'start_date'] = datetime.strptime(row['start_date'], '%Y-%m-%d %H:%M:%S %Z')
        except:
            ads_data.loc[index, 'start_date'] = datetime.strptime(row['start_date'], '%m-%Y-%d %H:%M:%S %Z')
        
        try:
            ads_data.loc[index, 'date_loaded'] = datetime.strptime(row['date_loaded'], '%Y-%m-%d %H:%M:%S %Z')
        except:
            ads_data.loc[index, 'date_loaded'] = datetime.strptime(row['date_loaded'], '%m-%Y-%d %H:%M:%S %Z')
        
        try:
            ads_data.loc[index, 'end_date'] = datetime.strptime(row['end_date'], '%Y-%m-%d %H:%M:%S %Z')
        except:
            try:
                ads_data.loc[index, 'end_date'] = datetime.strptime(row['end_date'], '%m-%Y-%d %H:%M:%S %Z')
            except:
                # Hay filas con TT en vez de los minutos, analizando siempre el end date es casi el mismo que el start_date, por eso lo reemplazo con este y agrego 2 minutos
                ads_data.loc[index, 'end_date'] = datetime.strptime(row['start_date'], '%Y-%m-%d %H:%M:%S %Z')
    
    # Hay algunos registros con * y +, los quito con un replace antes de procesarlos
    try:
        analytics_data['date'] = analytics_data['date'].replace({'\*': ''}, regex=True)
        analytics_data['date'] = analytics_data['date'].replace({'\+': ''}, regex=True)
    except:
        print('')
    
    for index, row in analytics_data.iterrows():
        #Hay casos en que el dia esta primero, en la mayoria de casos el YYYY. Por eso, reviso si empieza con 2, es ano y no mes
        if str(row['date']).startswith('2'):
            analytics_data.loc[index, 'date'] = datetime.strptime(str(row['date']), '%Y%m%d')
        else:
            analytics_data.loc[index, 'date'] = datetime.strptime(str(row['date']), '%d%m%Y')
            
    GuardarDataProcesada()

def MejorarConsistenciaDatos():
    global ads_data, analytics_data
    #Verifiquemos los datos unicos de source, para identificar consistencia
    print('Sources unicas, antes de realizar cambios: \n')
    print(analytics_data.source.unique(), '\n')
    #Vamos a reemplazar los valores mas comunes
    reemplazar_similares(analytics_data, 'source', 'facebook.com')
    reemplazar_similares(analytics_data, 'source', 'google.com')
    reemplazar_similares(analytics_data, 'source', 'yahoo.com')
    print('Sources unicas, despues de realizar cambios: \n')
    print(analytics_data.source.unique(), '\n')
    
def reemplazar_similares(df, column, string_to_match, min_ratio = 80):
    # obtener una lista de strings
    strings = df[column].unique()
    # obtener los 20 primeros resultados cercanos
    matches = fuzzywuzzy.process.extract(string_to_match, strings, 
                                         limit=20, scorer=fuzzywuzzy.fuzz.token_sort_ratio)
    # obtener solo los resultados con un ratio > 90
    close_matches = [matches[0] for matches in matches if matches[1] >= min_ratio]
    # get the rows of all the close matches in our dataframe
    rows_with_matches = df[column].isin(close_matches)
    # replace all rows with close matches with the input matches 
    df.loc[rows_with_matches, column] = string_to_match
    
def GuardarDataProcesada():
    #Vamos a guardar la data en nuestro archivo H5, para no tener que procesarlo nuevamente
    global ads_data, analytics_data, store
    store = pd.HDFStore('store.h5')
    store['ads_data'] = ads_data
    store['analytics_data'] = ads_data
    
def exportarBigQuery():
    global ads_data, analytics_data, dfSql
    # Exportamos los DataFrames a Google BigQuery usando pandas_gbq, reemplazamos si existe
    # Se suben los datos a 
    pandas_gbq.to_gbq(ads_data, 'test_data.ads_data', project_id='resolve-dataengineer-test', if_exists='replace')
    pandas_gbq.to_gbq(analytics_data, 'test_data.analytics_data', project_id='resolve-dataengineer-test', if_exists='replace')
    pandas_gbq.to_gbq(dfSql, 'test_data.analisis_data', project_id='resolve-dataengineer-test', if_exists='replace')
    
def CrearCodigoSqlDesdeDataframe(df, tabla):
    # Creemos un SQL statement a partir de dataframes
    sql_create = pd.io.sql.get_schema(df.reset_index(), tabla)
    sql_insert = []
    for index, row in df.iterrows():
        sql_insert.append('INSERT INTO '+tabla+' ('+ str(', '.join(df.columns))+ ') VALUES '+ str(tuple(row.values)))
    sql_final = str(sql_create)+'\n\n'+str(sql_insert)
    print(sql_final)
    
#
print('El análisis de los datos, antes de procesarlos es: \n')
calcularMetricas()
MejorarConsistenciaDatos()
#procesarFechas()
#Exportar a BigQuery directamente al projecto
exportarBigQuery()
#Calculo de metricas despues de procesar fechas
print('El análisis de los datos, despues de procesarlos es: \n')
calcularMetricas()
#Imprime el codigo SQL generado del dataframe 
CrearCodigoSqlDesdeDataframe(dfSql, 'analisis_data')