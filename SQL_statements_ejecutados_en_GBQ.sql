## Filas totales ads
SELECT count(*) as filas_totales_data
FROM test_data.ads_data;

## Filas totales analytics
SELECT count(*) as filas_totales_analytics
FROM test_data.analytics_data;

###### ADS DATASET ######

## Ads por Dia, ordenados de mayor a menor
SELECT DATE(start_date), count(start_date) as Ads_por_dia
FROM test_data.ads_data
GROUP BY start_date
ORDER BY Ads_por_dia DESC

## Ads por medio
SELECT medium, count(medium) as Ads_por_medio
FROM test_data.ads_data
GROUP BY medium

## Inversion promedio en ads por support, ordenado desc
SELECT support, AVG(inversion) as inversion_promedio
FROM test_data.ads_data
GROUP BY support
ORDER BY inversion_promedio DESC

## Rating promedio por formato
SELECT format, AVG(rating) as rating_promedio
FROM test_data.ads_data
GROUP BY format
ORDER BY format

## Total inversion por referencia
SELECT reference, SUM(inversion) as Total_inversion
FROM test_data.ads_data
GROUP BY reference
ORDER BY Total_inversion DESC

##### ANALYTICS DATASET ######

## conteo total por source
SELECT source, count(source) as Total_por_source
FROM test_data.analytics_data
GROUP BY source
ORDER BY Total_por_source DESC

## numero de sistemas operativos unicos
SELECT count(distinct operatingSystem) as Sistemas_operativos_unicos
FROM test_data.analytics_data

## Total paginas visitadas y nuevas visitas por pais
SELECT country, sum(pageviews) as Total_paginas_vistas, sum(newVisits) as Total_nuevas_visitas
FROM test_data.analytics_data
GROUP BY country
ORDER BY Total_paginas_vistas DESC

## Numero de visitas por navegador y fuente
SELECT source,browser,count(browser) as Suma_navegador
FROM test_data.analytics_data
GROUP BY source, browser
ORDER BY Suma_navegador DESC
