
## Alexander Benitez - Data Engineer Test Result

Buen dia, gracias por permitirme participar.

**Mi proyecto esta compuesto por los siguientes files**

1. DataEngineerTest.py - Este es un script en Python procesa los archivos Ads_schedule.csv y Analytics.csv, genera las estadisticas de los datos, procesa las fechas y sube la data a GBQ.
2. execution.log - Este es el log de ejecuccion del script
3. Prueba Teorica Data Engineer - Alexander Benitez.docx - La prueba teorica requerida
4. sql_statement_creado.sql - Este es el SQL statement creado por el codigo en DataEngineerTest.py
5. store.h5 - Archivo HDF5 para guardar los datos procesados por DataEngineerTest.py
6. Test Animodos 2020 - Alexander Benitez.xlsx - Test requerido
7. requirements.txt - Los modulos de python requeridos para correr el script
8. **CARPETA** /data-engineer-python-docker hay un Dockerfile y los requeriments.txt para generar el contenedor.
9. SQL_statements_ejecutados_en_GBQ.txt/sql - Aqui estan todos los comandos exploratios ejecutados en GBQ
10. google_compute_engine.pub - SSH Public key para acceder a 35.227.117.147, una VM que tiene un Jupyter Notebook en data-engineer-test, en donde estan todos los calculos realizados en GBQ

Para probar el codigo de parsing de las fechas, es aconsejable limitar el numero de filas a importar, debido a el tiempo que toma en procesarlas.

Quedo atento a cualquier comentario.
