
CREATE TABLE "analisis_data" (
"index" INTEGER,
  "Dataset" TEXT,
  "Forma" TEXT,
  "Celdas_totales" INTEGER,
  "Celdas_faltantes" REAL,
  "Porcentaje_faltantes" REAL,
  "Proporcion_duplicados" REAL
)

INSERT INTO analisis_data (Dataset, Forma, Celdas_totales, Celdas_faltantes, Porcentaje_faltantes, Proporcion_duplicados) VALUES ('Ads', (75229, 13), 977977, 0.1569566564448857, 0.1569566564448857, 0.0),
INSERT INTO analisis_data (Dataset, Forma, Celdas_totales, Celdas_faltantes, Porcentaje_faltantes, Proporcion_duplicados) VALUES ('Analytics', (908148, 15), 13622220, 24.2086972607989, 24.2086972607989, 33.210445874460994)
